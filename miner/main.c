#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "siggen.h"
#include "siggen.c"
#include "linkedList.h"

/*
 * Global variables used for events
 */
int prev_id = 0;
int id = 1;
unsigned int prev_sig = 0;
unsigned int block_nonce = 0;

/*
 * Returns the signature calculated using functions from siggen.c
 * Params:
 *      mine_transact:  Linked list containing transactions of the block
 *                      the signature is calculated for
 *      num_tr:         The number of transactions in the block
 * Returns: unsigned int value representing the signature for the
 *          block
 */
unsigned int calculate_sig(mempool_t *mine_transact, int num_tr) {
    unsigned int sig;
    sig = siggen_new();
    sig = siggen_int(sig, id);
    sig = siggen_int(sig, prev_id);
    sig = siggen_int(sig, prev_sig);
    sig = siggen_int(sig, num_tr);

    node_t *temp = mine_transact->head;
    while (temp != NULL) {
        transaction_t t = temp->trx;
        sig = siggen_int(sig, t.tid);
        sig = siggen_string(sig, t.payer);
        sig = siggen_string(sig, t.payee);
        sig = siggen_int(sig, t.amount);
        sig = siggen_int(sig, t.fee);
        temp = temp->next;
    }
    int sigBeforeNonce = sig;
    block_nonce = 0;

    while (1) {
        sig = siggen_int(sigBeforeNonce, block_nonce);
        if (sig < 16777215) {
            break;
        }
        block_nonce++;
    }
    return sig;
}

int main() {
    char event[5];
    mempool_t *list = new_mempool();
    unsigned int block_sig;

    while (scanf("%s", event) != -1) {

        // for the transaction event
        if (!strcmp(event, "TRX")) {

            transaction_t *transaction = new_transaction();

            // reading values from the input and assigning them
            // to the transaction struct
            scanf("%d %s %s %d %d", &(transaction->tid), transaction->payer,
                  transaction->payee, &(transaction->amount), &(transaction->fee));

            // adding transaction struct to the mempool
            // linked list
            list_append(list, transaction);

            printf("Adding transaction: %d %s %s %d %d\n", transaction->tid,
                   transaction->payer, transaction->payee, transaction->amount,
                   transaction->fee);

        } else if (!strcmp(event, "BLK")) {
            int num = 0;

            // Reading input for the BLK event
            scanf("%d %d %i %d ", &(id), &(prev_id), &(prev_sig), &(num));

            transaction_t *transact = new_transaction();
            int num_tr = num;

            // Reading transaction input for the block event
            // and removing those transactions from the mempool
            // if they exist in the mempool
            while (num_tr > 0) {
                scanf("%d %s %s %d %d", &(transact->tid), transact->payer,
                      transact->payee, &(transact->amount), &(transact->fee));

                transaction_t *toPrint = transact;

                if (delete_element(list, transact) == 1) {
                    printf("Removing transaction: %d %s %s %d %d\n", toPrint->tid,
                           toPrint->payer, toPrint->payee, toPrint->amount, toPrint->fee);
                }
                num_tr--;
            }
            scanf("%i %i", &(block_nonce), &(block_sig));

            // Updating variables for the next event
            prev_id = id;
            id = id + 1;
            prev_sig = block_sig;

        } else if (!strcmp(event, "MINE")) {

            // to create a list for transactions for mining
            mempool_t *mine_transact = new_mempool();

            node_t *temp = list->head;
            int length;
            int size = 0;
            int max_size = 232;
            int num_tr;

            // Adding transactions to the block
            while (temp != NULL) {

                // To calculate the size of each transaction
                transaction_t active = temp->trx;
                size = size + strlen(active.payee) + strlen(active.payer) + 14;
                if (size > max_size) {
                    break;
                }

                list_append(mine_transact, &(temp->trx));
                temp = temp->next;
            }

            // deleting the added transactions from the mempool
            length = list_len(mine_transact);
            while (length > 0) {
                node_t *removeFirst = remove_first(list);
                free(removeFirst);
                length--;
            }

            num_tr = list_len(mine_transact);
            block_sig = calculate_sig(mine_transact, num_tr);

            // Printing the final output for the MINE event
            printf("%s %d %d 0x%8.8x %d\n", "Block mined:", id, prev_id, prev_sig, num_tr);
            print_list(mine_transact);
            printf("0x%8.8x 0x%8.8x\n", block_nonce, block_sig);

            // Updating variables for the next event
            prev_id = id;
            id = id + 1;
            prev_sig = block_sig;

        } else if (!strcmp(event, "END")) {
            break;
        }
    }
    return 0;
}