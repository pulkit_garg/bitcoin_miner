//
// Created by pgarg on 19-05-2021.
// The starting code for this file has been
// adpoted from Lecture 2 which was written
// by Professor Alex Brodsky
//

#ifndef MINER_LINKEDLIST_H
#define MINER_LINKEDLIST_H

/*
 * This struct comprises of all the values
 * that are needed to store a transaction
 */
typedef struct transaction {
    unsigned int tid;
    char payer[32];
    char payee[32];
    unsigned int amount;
    unsigned int fee;
} transaction_t;

/*
 * This struct is used to create a node for
 * the linked list
 */
typedef struct node {
    struct node *next;
    transaction_t trx;
} node_t;

/*
 * This struct is used to make the linked list.
 * It comprises of pointers to head and tail of
 * the linked list
 */
typedef struct list {
    node_t *head;
    node_t *tail;
} mempool_t;

// The functions are listed in order as they appear
// in linkedList.c
extern mempool_t * new_mempool();
extern transaction_t * new_transaction();
extern int list_contains(mempool_t *list, transaction_t *b);
extern void list_append(mempool_t *list, transaction_t *b);
extern int list_len(mempool_t *list);
extern node_t * remove_first(mempool_t *list);
extern void print_list(mempool_t *list);
extern int delete_element(mempool_t *list, transaction_t *a);
extern int structcmp(transaction_t *a, transaction_t *b);

#endif //MINER_LINKEDLIST_H
