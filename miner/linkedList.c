//
// Created by pgarg on 19-05-2021.
// The starting code for this file has been
// adpoted from Lecture 2 which was written
// by Professor Alex Brodsky
// The file contains functions which are used
// to create and manipulate the linked list
//

#include "linkedList.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

/*
 * Returns the list struct with a space assigned
 * Params: None
 * Returns: list struct variable with the assigned
 *          memory
 */
extern mempool_t *new_mempool() {
    mempool_t *list = calloc(1, sizeof(mempool_t));
    assert(list != NULL);
    return list;
}

/*
 * Returns the transition struct with a space assigned
 * Params: None
 * Returns: transition struct variable with the assigned
 *          memory
 */
extern transaction_t *new_transaction() {
    transaction_t *mem = calloc(1, sizeof(transaction_t));
    assert(mem != NULL);
    return mem;
}

/*
 * Returns integer value based on if an element exists in a linked
 * list or not
 * Params:
 *      *list:  Pointer to the list where the transaction struct
 *              element has to be searched
 *      *b:     Pointer to the transition struct which has to be
 *              searched in the list
 * Returns: int value to determine if a transition struct exists
 *          in the linked list (0 if it does not, 1 if it does)
 */
extern int list_contains(mempool_t *list, transaction_t *b) {
    assert(list != NULL);

    for (node_t *ptr = list->head; ptr != NULL; ptr = ptr->next) {
        transaction_t *a = &(ptr->trx);
        if (structcmp(a, b)) {
            return 1;
        }
    }
    return 0;
}

/*
 * This method is used to append a node object with a transaction
 * struct value to the mempool_t linkedlist
 * Params:
 *      *list:  Pointer to the list where the transaction struct
 *              element has to be appended
 *      *a:     Pointer to the transition struct which has to be
 *              appended to the list
 * Returns: None
 */
extern void list_append(mempool_t *list, transaction_t *a) {
    // check first if the list is not null
    assert(list != NULL);

    // assign a node object with the transition_t struct
    node_t *node = calloc(1, sizeof(node_t));
    assert(node != NULL);
    node->trx = *a;

    if (list->head == NULL) {
        list->head = node;
        list->tail = node;
    } else {
        list->tail->next = node;
        list->tail = node;
    }
}

/*
 * Returns the length of the linked list
 * Params:
 *      *list:  Pointer to the list whose length has
 *              to be calculated
 * Returns: Integer value representing the size
 *          of the linked list
 */
extern int list_len(mempool_t *list) {
    int count = 0;
    node_t *temp = list->head;
    while (temp != NULL) {
        count++;
        temp = temp->next;
    }
    return count;
}

/*
 * Returns the node struct variable which is removed from the
 * first position in the linked list
 * Params:
 *      *list:  Pointer to the list from which an element has to
 *              be removed from the first position
 * Returns: a node struct variable which is removed from the first
 *          position in the linked list
 */
extern node_t *remove_first(mempool_t *list) {
    assert(list != NULL);
    node_t *temp = list->head;
    list->head = list->head->next;
    return temp;
}

/*
 * This method is used to print all the elements in the linked list
 * with the info about the values in transition struct
 * Params:
 *      *list:  Pointer to the list whose elements have to be
 *              printed
 * Returns: None
 */
extern void print_list(mempool_t *list) {
    for (node_t *ptr = list->head; ptr != NULL; ptr = ptr->next) {
        printf("%d %s %s %u %d\n", ptr->trx.tid, ptr->trx.payer, ptr->trx.payee,
               ptr->trx.amount, ptr->trx.fee, ptr->trx.fee);
    }
}

/*
 * Returns an int value to determine if a node has been deleted
 * from the linked list or not
 * Params:
 *      *list:  Pointer to the list from which a node has to be
 *              deleted
 *      *elem:  Pointer to the transition struct which has to be
 *              deleted from the linked list
 * Returns: Returns an integer value 0 if the transition struct
 *          is not found in the list. Returns an integer value 1
 *          if the transition struct is found and removed from the
 *          linked list
 * References: Following websites have been referred to write this
 *             method:
 *             [1] https://www.programiz.com/dsa/linked-list-operations
 *             [2] https://www.geeksforgeeks.org/linked-list-set-3-deleting-node/
 */
extern int delete_element(mempool_t *list, transaction_t *elem) {

    if (list->head == NULL) {
        return 0;
    }

    if (list->head->trx.tid == elem->tid) {
        if (list->head->next != NULL) {
            list->head = list->head->next;
            return 1;
        } else {
            list->head = NULL;
            return 1;
        }
    } else if (list->head->trx.tid != elem->tid && list->head->next == NULL) {
        return 0;
    }

    node_t *prev = list->head;
    node_t *curr = list->head;

    while (curr->next != NULL && curr->trx.tid != elem->tid) {
        prev = curr;
        curr = curr->next;
    }

    if (curr->trx.tid == elem->tid) {
        prev->next = prev->next->next;
        free(curr);
        return 1;
    } else {
        return 0;
    }
}

/*
 * Returns an int value by comparing if two transition structs
 * are equal or not
 * Params:
 *      *a:     Pointer to the first struct element which has to
 *              be compared
 *      *b:     Pointer to the second struct element which has to
 *              be compared
 * Returns: An integer value of 1 if the values under similar variables
 *          are equal in the structs. Returns an integer value 0 if
 *          they are not
 */
extern int structcmp(transaction_t *a, transaction_t *b) {
    if (a->amount == b->amount && a->tid == b->tid &&
        a->fee == b->fee && !strcmp(a->payee, b->payee)
        && !strcmp(a->payer, b->payer)) {
        return 1;
    }
    return 0;
}
